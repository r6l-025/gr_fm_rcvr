################################################################################
# Вспомогательные функции
################################################################################

###################################################
# Функция автоматического поиска C исходников в директории src_path. Функция
# ищет файлы по маскам, и возвращает результат через переменные в кеше.
#
# @param header_var_name  - имя переменной в которую будет занесен результат
#                           поиска хедеров, и сохранен в кэше
# @param src_var_name     - имя переменной в которую будет занесен результат
#                           поиска исходников, и сохранен в кэше
# @param src_path         - путь к директории поиска относительно директории
#                           с обрабатываемым файлом CMakeList
#
###################################################
function(
    auto_search_files_C
    header_var_name
    src_var_name
    src_path
    )

    # автоматический поиск заголовочных файлов
    file (GLOB _headr_group ${src_path}/*.h)
    set(${header_var_name} ${_headr_group} CACHE STRING "C headers" FORCE)

    # автоматический поиск исходников
    file (GLOB _src_group ${src_path}/*.c)

    set(${src_var_name} ${_src_group} CACHE STRING "C src" FORCE)
endfunction()

###################################################
# Функция автоматического поиска CXX исходников в директории src_path. Функция
# ищет файлы по маскам, и возвращает результат через переменные в кеше.
#
# @param header_var_name  - имя переменной в которую будет занесен результат
#                           поиска хедеров, и сохранен в кэше
# @param src_var_name     - имя переменной в которую будет занесен результат
#                           поиска исходников, и сохранен в кэше
# @param src_path         - путь к директории поиска относительно директории
#                           с обрабатываемым файлом CMakeList
#
###################################################
function(
    auto_search_files_CXX
    header_var_name
    src_var_name
    src_path
    )
    # автоматический поиск заголовочных файлов
    file (GLOB _headr_group ${src_path}/*.h)
    set(_headr ${_headr_group})

    file (GLOB _headr_group ${src_path}/*.hpp)
    list(
        APPEND _headr
        ${_headr_group}
        )
    file (GLOB _headr_group ${src_path}/*.hxx)
    list(
        APPEND _headr
        ${_headr_group}
        )
    file (GLOB _headr_group ${src_path}/*.h++)
    list(
        APPEND _headr
        ${_headr_group}
        )
    file (GLOB _headr_group ${src_path}/*.hh)
    list(
        APPEND _headr
        ${_headr_group}
        )
    set(${header_var_name} ${_headr} CACHE STRING "CXX headers" FORCE)

    # автоматический поиск исходников
    file (GLOB _src_group ${src_path}/*.cpp)
    set(_src ${_src_group})

    file (GLOB _src_group ${src_path}/*.cxx)
    list(
        APPEND _src
        ${_src_group}
        )
    file (GLOB _src_group ${src_path}/*.c++)
    list(
        APPEND _src
        ${_src_group}
        )
    set(${src_var_name} ${_src} CACHE STRING "CXX src" FORCE)

endfunction()

###################################################
# Функция автопоиска UI файлов Qt проекта 
#
# Возвращает результат через переменные в кэше.
#
# @param ui_var_name - имя переменной в которую будет занесен результат
#                      поиска ui, и сохранен в кэше
# @param ui_path     - путь к директории поиска относительно директории
#                      с обрабатываемым файлом CMakeList
#
###################################################
function(
    auto_search_files_QTUI
    ui_var_name
    ui_path
    )

    # автоматический поиск ui файлов
    file (GLOB _ui_group ${ui_path}/*.ui)
    set(${ui_var_name} ${_ui_group} CACHE STRING "qt-ui" FORCE)

endfunction()

###################################################
# Функция подготовки цели сборки
#
# Сообщает системе сбкори какие инклюдники потребуются проекту,
# и скакми библиотеками необходимослинковать цель
#
# @param __trgt_name - имя цели сборки
#
# @note функция зависит от переменных TRGT_INCLUDE и TRGT_LINKS из
#       родительской области сидимости
#
###################################################
function(
    prepare_target
    __trgt_name
    )

if(TRGT_INCLUDE)
    # указываем на список хедеров которые компилятору слудет подключить для сборки
    target_include_directories(${__trgt_name} PRIVATE ${TRGT_INCLUDE})
endif()

if(TRGT_LINKS)
    # указываем список библиотек с которыми нужно слинковаться для сборки
    target_link_libraries(${__trgt_name} PRIVATE ${TRGT_LINKS})
endif()
endfunction()

###################################################
# Функция линковки Qt библиотек к указанной цели
#
# Получает ИМЯ(!) переменной хранящей список qt библиотек и линкует их к цели
# имя которой передано через аргумент.
#
# @WARNING! список передается по имени а не по значению. Иначе список
# разворачивается в набор аргментов, и в нужный аргумент функции попадает только
# первый жлемент списка
#
# @param __target_name      - имя цели к которой линкуем;
# @param __qt_packs_list    - имя переменной списка которую разыменуем для
#                             получения самого списка
#
###################################################
function(
    prepare_target_QT
    __target_name
    __qt_packs_list
    )

    unset (QT_LIBS_LIST)

    foreach (QT_LIB ${${__qt_packs_list}})
        list(
            APPEND QT_LIBS_LIST
            "Qt5::${QT_LIB}"
            )
    endforeach (QT_LIB)

    target_link_libraries(
        ${__target_name}
        PRIVATE
        ${QT_LIBS_LIST}
        )

endfunction()

###################################################
# Функция линковки Qt библиотек ко всем целям проекта
#
# Получает ИМЯ(!) переменной хранящей список qt библиотек и линкует их ко всем
# целям проекта
#
# @WARNING! список передается по имени а не по значению. Иначе список
# разворачивается в набор аргментов, и в нужный аргумент функции попадает только
# первый жлемент списка
#
# @param __qt_packs_list    - имя переменной списка которую разыменуем для
#                             получения самого списка
#
###################################################
function(
    prepare_libs_QT
    __qt_packs_list
    )

    unset (QT_LIBS_LIST)

    foreach (QT_LIB ${${__qt_packs_list}})
        list(
            APPEND QT_LIBS_LIST
            "Qt5::${QT_LIB}"
            )
    endforeach (QT_LIB)

    link_libraries(
        ${QT_LIBS_LIST}
        )

endfunction()
    
