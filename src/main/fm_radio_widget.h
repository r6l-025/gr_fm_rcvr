#ifndef GR_FM_RCVR_FM_RADIO_WIDGET_H
#define GR_FM_RCVR_FM_RADIO_WIDGET_H

/* Qt includes */
#include <QMainWindow>
#include <QGridLayout>
#include <QTabWidget>
#include <QWidget>
#include <QPushButton>
#include <QSystemTrayIcon>

/* GR includes */
#include <gnuradio/analog/sig_source.h>
#include <gnuradio/audio/sink.h>
#include <gnuradio/blocks/multiply.h>
#include <gnuradio/filter/fir_filter_blk.h>
#include <gnuradio/filter/firdes.h>
#include <gnuradio/filter/rational_resampler_base.h>
#include <gnuradio/top_block.h>
#include <gnuradio/qtgui/waterfall_sink_c.h>

#include <osmosdr/source.h>

/* Local includes */
#include "ui_main_form.h"
#include "fm_demod/wbfw_demod_cf.h"

using namespace gr;

class fm_radio_widget : public QMainWindow
                      , private Ui_MainWindow
{
    Q_OBJECT
public: /* структуры данных */
    /**
     * @brief Структура конфигов
     */
    struct fmr_settings
    {
        /** громкость в процентах */
        int     volume;
        /** частота в МГц */
        double  rfrq;
    };
    
public: /* методы */
    fm_radio_widget(
                  const fmr_settings&   settings
                , QWidget*              parent = nullptr
                );
    ~fm_radio_widget();
    
    /**
     * @brief Метод старта GR приложения
     */
    void start()
    {
        _tb->start();
    }
    /**
     * @brief Загрузка конфигов из ini файла
     */
    void set_settings(
                const fmr_settings&   settings
                );
    
    /**
     * @brief Сохранение конфигов в ini файл
     */
    void get_settings(
                fmr_settings*   settings
                );


private: /* методы */
    /**
     * @brief Инициализация ресурсов GnuRadio
     */
    void _init_gr();
    
    /**
     * @brief Коммутация блоков между собой
     */
    void _connct_blocks();
    
    /**
     * @brief Установка состояния чек-бокса включения водопада
     *
     * @param state - устанавливаемое состояние
     */
    void _set_cb_waterflow(bool state);
    
    /**
     * @brief Установка/разрыв соединения приемника с виджетом водопада
     *
     * @param new_state новое состояние. true - установить соединение
     */
    void _connect_waterfall_stream(bool new_state);
    
    /**
     * @brief Включение/выключение отбражения виджета водопада
     *
     * @note только отборажение, потоки данных не затрагиваются
     *
     * @param new_state - уствнавливаемое состояние видимости
     */
    void _show_eaterfall_widget(bool new_state);
    
    /**
     * @brief Вычисление коэффициентов анти-алиасингового фильтра ресемплера
     *
     * @param interpolation - коэффициент интерполяции;
     * @param decimation    - коэффициент децимации;
     * @param fractional_bw - относительная ширина полосы пропускания фильтра
     *                        [0:0.5] от общей ширины полосы
     *
     * @return вектор коэффициентов
     */
    std::vector<float> _design_resampler_filter(
                                      const unsigned  interpolation
                                    , const unsigned  decimation
                                    , const float     fractional_bw
                                    );
    void _prepare_osmodir();
    void _prepare_resampler();
    void _prepare_waterfall();
    void _prepare_LP_FIR();
    
    /**
     * @brief Установка уровня звука (в процентах) в соответствующий блок
     *
     * @param new_percent_val
     */
    void set_volume(float new_percent_val);
    
    /**
     * @brief Обработчик события закрытия виджета
     *
     * @param event - указатель на объект события
     */
    void closeEvent(QCloseEvent* event);
    
    /**
     * @brief Настройка виджета для сворачивания приложения в трей
     */
    void _make_tray_magic();
    
private: /* переменные */
    /** частота дискретизации сигнала на выходе приемника (Гц) */
    const uint32_t                  _rcvr_wb_fs = (int)3.2e6;
    /** частота дискретизации не демодулированного сигнала (выход LPF) (Гц) */
    const uint32_t                  _wb_fs      = (int)320e3;
    /** частота дискретизации аудиосигнала (Гц) */
    const uint32_t                  _audio_fs   = (int)32e3;
    /** коэффициент децимации в демодуляторе */
    const uint32_t                  _wbfm_dec_factor = 10;
    /** частота среза FIR фильтра (Гц) */
    const uint32_t                  _fir_cutt_freq = (int)10e3;
    /** частота подавления FIR фильтра (Гц) */
    const uint32_t                  _fir_stop_freq = (int)120e3;
    /** частота приема по умолчанию */
    const double                    _default_rcvr_freq = 73.76e6;
    
    /** флаг указывающий замьютить звук */
    bool                            _mute_audio;
    /** флаг указывающий отобразить водопад */
    bool                            _show_waterfall;
    /** относительное значение уровня громкости аудиосигнала */
    int                             _audio_volume;
    /** объект топ-блока GR */
    top_block_sptr                  _tb;
    /** указатель на GrQt виджет водопада */
    qtgui::waterfall_sink_c::sptr   _waterflw_sink;
    /** указатель на блок взаимодействия с подсистемой аудио */
    audio::sink::sptr               _audio_sink;
    /** указатель на FIR фильтр */
    filter::fir_filter_ccf::sptr    _filter;
    /** указатель на блок управления железом */
    osmosdr::source::sptr           _osmosdr;
    /** указатель на блок FM демодулятора */
    fm_demod::wbfw_demod_cf::sptr   _wbfm_demod;
    /** указатель на блок ресемплера */
    filter::rational_resampler_base_fff::sptr _resampler;
    /** указатель на блок векторного умножения */
    blocks::multiply_ff::sptr       _mult;
    /** указатель на константный вектор */
    analog::sig_source_f::sptr      _const_v;
    
    /* Объявляем объект будущей иконки приложения для трея */
    QSystemTrayIcon*                trayIcon;
    
public slots: /* слоты */
    
    /**
     * @brief слот для подготовки к завершению программы
     */
    void atexit_slot();
    
    /**
     * @brief слот обрабаывающий сигнал нажатия кнопки показа водопада
     */
    void show_wtflow_slot(bool);
    
    /**
     * @brief слот для обработки сигнала от кнопки завершения приложения
     */
    void quit_slot();
    
    /**
     * @brief слот изменения частоты приема
     *
     * @param val_in_persent - RF частота в МГц
     */
    void change_freq_slot(double new_freq_MHz);
    
    /**
     * @brief слот к слайдеру изменения громкости
     *
     * @param val_in_persent - громкость в процентах
     */
    void change_volume_slot(int);
    
    /**
     * @brief Слот обрабаывающий сигнал от кнопки мьюта звука
     */
    void mute_audio_slot(bool);
    
    /**
     * @brief Слот обрабатывания сигнала нажатия на иконку приложения в трее
     */
    void tray_icon_activated(QSystemTrayIcon::ActivationReason);
};


#endif //GR_FM_RCVR_FM_RADIO_WIDGET_H
