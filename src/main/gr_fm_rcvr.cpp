#include <stdlib.h>

/* Qt includes */
#include <QApplication>
#include <QFile>
#include <QSettings>
#include <QDebug>

/* Local includes */
#include "fm_radio_widget.h"

const double default_freq           = 73.76;
const int    default_volume_percent = 50;
const char*  settings_fname         = "/fm_radio_settings.ini";
QString      settings_pname;
fm_radio_widget* fm_widget;

/**
 * @brief Чтение конфигов из *.ini файла
 *
 * @return Структура с конфигами
 */
fm_radio_widget::fmr_settings get_settings_from_ini()
{
    fm_radio_widget::fmr_settings settings;
    
    settings_pname =   QApplication::applicationDirPath()
                     + QString(settings_fname);
    QSettings ini(
          settings_pname
        , QSettings::IniFormat
        );
    ini.setIniCodec("UTF-8");
    ini.sync();
    
    /*
     * Прочтем частоту
     */
    settings.rfrq = ini.value(
          "Global/rf_freq"
        , default_freq
        ).toReal();
    ini.setValue(
          "Global/rf_freq"
        , settings.rfrq
        );
    
    /*
     * Прочтем уровень громкости
     */
    settings.volume = ini.value(
          "Global/volume_percent"
        , default_volume_percent
        ).toInt();
    ini.setValue(
          "Global/volume_percent"
        , settings.volume
        );
    
    ini.sync();
    
    return settings;
}

/**
 * @brief Запись конфигов в *.ini файл
 *
 * @param settings - ссылка на структуру конфигов
 */
void set_settings_to_ini(
            const fm_radio_widget::fmr_settings& settings
            )
{
    QSettings ini(
          settings_pname
        , QSettings::IniFormat
        );
    ini.setIniCodec("UTF-8");
    ini.sync();
    
    /*
     * Сохраним конфиги частоты
     */
    ini.setValue(
          "Global/rf_freq"
        , settings.rfrq
        );
    
    /*
     * Сохраним конфиги громкости
     */
    ini.setValue(
          "Global/volume_percent"
        , settings.volume
        );
    
    ini.sync();
}

/**
 * @brief Функция созранения конфигов
 */
void atexit_handler()
{
    fm_radio_widget::fmr_settings settings;
    fm_widget->get_settings(&settings);
    set_settings_to_ini(settings);
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    
    fm_widget = new fm_radio_widget(
                            get_settings_from_ini()
                            );
    /* зарегестрируем обработчик сохраняющий параметры в ini при выходе */
    atexit(atexit_handler);
    
    /* разрешим выходить из приложения по сигналу */
    fm_widget->setAttribute(
        Qt::WA_QuitOnClose,
        true
        );
    
    /* установим стиль */
    QFile styleF(
            QString(":/qss/style.css")
            );
    styleF.open(
            QFile::ReadOnly
            );
    QString qssStr = styleF.readAll();
    qApp->setStyleSheet(qssStr);
    QIcon winImg(":/images/wave.png");
    fm_widget->setWindowIcon(
        winImg
        );
    
    /* запустим поток GR */
    fm_widget->start();
    
    /* отрисовка виджета */
    fm_widget->show();
    
    app.exec();
    
    return 0;
}

