/* Boost includes */
#include <boost/integer/common_factor_rt.hpp>
/* Qt includes */
#include <QCloseEvent>
#include <QMenu>
#include <QStyle>
#include <QAction>
#include <QMessageBox>
/* local inludes */
#include "fm_radio_widget.h"

using namespace gr;

/******************************************************************************/
fm_radio_widget::fm_radio_widget(
                              const fmr_settings&   settings
                            , QWidget*              parent
                            )  : QMainWindow(parent)
                                               , _mute_audio(false)
                                               , _show_waterfall(false)
                                               , _audio_volume(0)
{
    /* спрячем заголовок окна с кнопками */
    setWindowFlags(
          Qt::Dialog
        | Qt::WindowTitleHint
        | Qt::WindowCloseButtonHint
        | Qt::WindowSystemMenuHint
        | Qt::FramelessWindowHint
        );
    
    setupUi(this);
    
    /*
     * установим небольшую высоту для окна виджета чтобы при сворачивании
     * виджета водопада приложение автоматически сжималось по вертикали
     */
    setMaximumHeight(200);
    
    _tb = make_top_block("FM_receiver");
    _init_gr();
    set_settings(settings);
    _connct_blocks();
    
    /* установим чек-бокс выбора водопада в выключенное состояние */
    _set_cb_waterflow(false);
    /* по дефолту отключим виджет водопада */
    _show_eaterfall_widget(false);
    glay_waterfall->addWidget(
                          _waterflw_sink->qwidget()
                        , 0
                        , 0
                        );
    
    sb_freq->setToolTip("Central radio frequency");
    hs_volume->setToolTip("Audio volume");
    
    connect(
          pb_exit_2
        , &QPushButton::clicked
        , this
        , &fm_radio_widget::quit_slot
        );
    connect(
          cb_show_wtfall
        , &QCheckBox::toggled
        , this
        , &fm_radio_widget::show_wtflow_slot
        );
    connect(
          sb_freq
        , QOverload<double>::of(&QDoubleSpinBox::valueChanged)
        , this
        , &fm_radio_widget::change_freq_slot
        );
    connect(
          hs_volume
        , &QAbstractSlider::valueChanged
        , this
        , &fm_radio_widget::change_volume_slot
        );
    connect(
          cb_mute
        , &QCheckBox::toggled
        , this
        , &fm_radio_widget::mute_audio_slot
        );
    
    _make_tray_magic();
}

/******************************************************************************/
fm_radio_widget::~fm_radio_widget()
{
    disconnect(
          pb_exit_2
        , &QPushButton::clicked
        , this
        , &fm_radio_widget::quit_slot
        );
    disconnect(
          cb_show_wtfall
        , &QCheckBox::toggled
        , this
        , &fm_radio_widget::show_wtflow_slot
        );
    disconnect(
          sb_freq
        , QOverload<double>::of(&QDoubleSpinBox::valueChanged)
        , this
        , &fm_radio_widget::change_freq_slot
        );
    disconnect(
          hs_volume
        , &QAbstractSlider::valueChanged
        , this
        , &fm_radio_widget::change_volume_slot
        );
    disconnect(
          cb_mute
        , &QCheckBox::toggled
        , this
        , &fm_radio_widget::mute_audio_slot
        );
}

/******************************************************************************/
void fm_radio_widget::_make_tray_magic()
{
    cb_roll->setToolTip("Minimize application to tray instead of closing");
    /*
     * Инициализируем иконку трея, устанавливаем иконку из набора системных иконок,
     * а также задаем всплывающую подсказку
     */
    trayIcon = new QSystemTrayIcon(this);
    QIcon trayImg(":/images/wave.png");
    trayIcon->setIcon(
                trayImg
                );
    trayIcon->setToolTip("FM receiver");
    
    /* После чего создаем контекстное меню из двух пунктов*/
    auto menu       = new QMenu(this);
    auto quitAction = new QAction(
                              "Exit"
                            , this
                            );
    connect(
          quitAction
        , &QAction::triggered
        , this
        , &QApplication::quit
        );
    
    menu->addAction(quitAction);
    
    /*
     * Устанавливаем контекстное меню на иконку и показываем иконку приложения
     * в трее
     */
    trayIcon->setContextMenu(menu);
    trayIcon->show();
    
    /*
     * Также подключаем сигнал нажатия на иконку к обработчику данного нажатия
     */
    connect(
          trayIcon
        , &QSystemTrayIcon::activated
        , this
        , &fm_radio_widget::tray_icon_activated
        );
}

/******************************************************************************/
void fm_radio_widget::set_settings(
                            const fmr_settings&   settings
                            )
{
    change_freq_slot(settings.rfrq);
    sb_freq->setValue(settings.rfrq);
    
    change_volume_slot(settings.volume);
    hs_volume->setValue(settings.volume);
}

/******************************************************************************/
void fm_radio_widget::get_settings(
                            fmr_settings*   settings
                            )
{
    settings->rfrq   =   _osmosdr->get_center_freq()
                       / 1e6;
    settings->volume = _audio_volume;
}

/******************************************************************************/
void fm_radio_widget::_init_gr()
{
    /* подготовим водопад */
    _prepare_waterfall();
    /* подготовим аудио блок */
    _audio_sink = audio::sink::make((int)_audio_fs);
    /* подготовим блок управления приемником */
    _prepare_osmodir();
    /* подготвим входной ФНЧ фильтр */
    _prepare_LP_FIR();
    
    /* подготовим блок передискретизации */
    /*
     * Закомментируем блок передискретизации т.к. при данном соотношении частот
     * дискретизации он не нужен
     *
     * _prepare_resampler();
     */
    
    /* создаем демодулятор */
    _wbfm_demod = fm_demod::wbfw_demod_cf::make(
                                              (float)_wb_fs
                                            , (int)_wbfm_dec_factor
                                            );
    /* создаем блок умножения аудиосигнала */
    _mult = blocks::multiply_ff::make();
    /* создаем вектор констант для регулирования уровня звука */
    _const_v = analog::sig_source_f::make(0, analog::GR_SIN_WAVE, 0, 0, 0.3f);
}

/******************************************************************************/
void fm_radio_widget::_prepare_osmodir()
{
    /*
     * Т.к. при инициализации приемного устройства происходит попытка доступа
     * к самому железу - то вполне вероятно она может быть провальной.
     * Поэтому будем отлавливать исключения, и выводить причину в окно
     * сообщения пользователю
     */
    try
    {
        _osmosdr = osmosdr::source::make("buffers=512");
    }
    catch(std::exception& e)
    {
        QMessageBox::information(
                              this
                            , "Error"
                            , e.what()
                            , QMessageBox::Ok
                            );
        QApplication::quit();
    }
    
    _osmosdr->set_time_unknown_pps(osmosdr::time_spec_t());
    _osmosdr->set_sample_rate(_rcvr_wb_fs);
    /* установка дефолтной частоты */
    _osmosdr->set_center_freq(_default_rcvr_freq, 0);
    /* отключим корректировку частоты */
    _osmosdr->set_freq_corr(0);
    /* отключим корректировку постояного смещения сигнала */
    _osmosdr->set_dc_offset_mode(0);
    /* отключим балансировку квадратур */
    _osmosdr->set_iq_balance_mode(0);
    /* отключим АРУ */
    _osmosdr->set_gain_mode(true);
    /* дефолтное усиление в дБ */
    _osmosdr->set_gain(25);
    /* дефолтное усиление промежуточной частоты */
    _osmosdr->set_if_gain(20);
    _osmosdr->set_bb_gain(20);
    _osmosdr->set_antenna("");
    /* установим полосовой фильтр как "автоматический" */
    _osmosdr->set_bandwidth(0);
}

/******************************************************************************/
void fm_radio_widget::_prepare_resampler()
{
    auto decim  = (unsigned)_audio_fs;
    /* вычислим частоту дискретизации сигнала после демодулятора */
    auto interp =   _wb_fs
                  / _wbfm_dec_factor;
    assert(interp > decim);
    /* найдем НОД коэфф. интерполяции и децимации */
    auto d = boost::integer::gcd(interp, decim);
    interp /= d;
    decim  /= d;
    auto resempler_taps = _design_resampler_filter(
                                              interp
                                            , decim
                                            , 0.4f
                                            );
    
    _resampler = filter::rational_resampler_base_fff::make(
                                              interp
                                            , decim
                                            , resempler_taps
                                            );
}

/******************************************************************************/
void fm_radio_widget::_prepare_waterfall()
{
    _waterflw_sink = qtgui::waterfall_sink_c::make(
                                                  1024
                                                , fft::window::WIN_HANN
                                                , 0
                                                , _rcvr_wb_fs
                                                , ""
                                                , 1
                                                );
    _waterflw_sink->set_update_time(0.15);
    _waterflw_sink->enable_axis_labels(false);
    _waterflw_sink->enable_menu(true);
    _waterflw_sink->set_intensity_range(-140, 0);
    _waterflw_sink->set_color_map(0, 5);
}

/******************************************************************************/
void fm_radio_widget::_prepare_LP_FIR()
{
/* подготовим коэффициенты фильтра */
    auto fir_taps = filter::firdes::low_pass(
                                      1
                                    , _rcvr_wb_fs
                                    , _fir_cutt_freq
                                    , _fir_stop_freq
                                    );
    /* создадим сам фильтр (вход - complex, выход - float) */
    auto decim_factor =   _rcvr_wb_fs
                        / _wb_fs;
    _filter = filter::fir_filter_ccf::make(
                                      (int)decim_factor
                                    , fir_taps
                                    );
}

/******************************************************************************/
void fm_radio_widget::_connct_blocks()
{
    _tb->connect(_osmosdr   , 0, _filter    , 0);
    _tb->connect(_filter    , 0, _wbfm_demod, 0);
    /*
     * Отключим в данной реализации блок передискретизации
     *  _tb->connect(_wbfm_demod, 0, _resampler , 0);
     *  _tb->connect(_resampler , 0, _mult      , 0);
     */
    _tb->connect(_wbfm_demod, 0, _mult, 0);
    _tb->connect(_const_v   , 0, _mult      , 1);
    _tb->connect(_mult      , 0, _audio_sink, 0);
}

/******************************************************************************/
void fm_radio_widget::_set_cb_waterflow(bool state)
{
    auto check_state =   state
                       ? Qt::Checked
                       : Qt::Unchecked;
    cb_show_wtfall->setCheckState(check_state);
}

/******************************************************************************/
void fm_radio_widget::_connect_waterfall_stream(bool new_state)
{
    if(new_state)
    {
        _tb->lock();
        _tb->connect(_osmosdr, 0, _waterflw_sink, 0);
        _tb->unlock();
        _show_eaterfall_widget(true);
    }
    else
    {
        _tb->lock();
        _tb->disconnect(_osmosdr, 0, _waterflw_sink, 0);
        _tb->unlock();
        _show_eaterfall_widget(false);
    }
}

/******************************************************************************/
void fm_radio_widget::_show_eaterfall_widget(bool new_state)
{
    if(new_state)
    {
        w_glay_wfall->show();
        _waterflw_sink->qwidget()->show();
    }
    else
    {
        _waterflw_sink->qwidget()->hide();
        w_glay_wfall->hide();
    }
}

/******************************************************************************/
std::vector<float> fm_radio_widget::_design_resampler_filter(
                                                  const unsigned  interpolation
                                                , const unsigned  decimation
                                                , const float     fractional_bw
                                                )
{
    
    if (fractional_bw >= 0.5 || fractional_bw <= 0)
    {
        throw std::range_error(
                        "Invalid fractional_bandwidth, must be in (0, 0.5)"
                        );
    }
    
    // These are default values used to generate the filter when no taps are known
    //  Pulled from rational_resampler.py
    float beta = 7.0;
    float halfband = 0.5;
    float rate = float(interpolation) / float(decimation);
    float trans_width, mid_transition_band;
    
    if (rate >= 1.0)
    {
        trans_width = halfband - fractional_bw;
        mid_transition_band = halfband - trans_width / 2.0f;
    }
    else
    {
        trans_width = rate * (halfband - fractional_bw);
        mid_transition_band = rate * halfband - trans_width / 2.0f;
    }
    
    return filter::firdes::low_pass(interpolation,       /* gain */
        interpolation,       /* Fs */
        mid_transition_band, /* trans mid point */
        trans_width,         /* transition width */
        filter::firdes::win_type::WIN_KAISER,
        beta
        ); /* beta*/
}

/******************************************************************************/
void fm_radio_widget::closeEvent(QCloseEvent* event)
{
    /*
     * Если окно видимо и чекбокс отмечен, то завершение приложения
     * игнорируется, а окно просто скрывается
     */
    if(isVisible() && cb_roll->isChecked())
    {
        event->ignore();
        hide();
        auto icon = QSystemTrayIcon::MessageIcon(
                                            QSystemTrayIcon::Information
                                            );
        
        trayIcon->showMessage(
              "FM receiver"
            , "App moving to tray"
            , icon
            , 2000
            );
    }
    else
    {
        event->accept();
        atexit_slot();
        QApplication::quit();
    }
}

/******************************************************************************/
void fm_radio_widget::tray_icon_activated(
                                QSystemTrayIcon::ActivationReason reason
                                )
{
    switch(reason)
    {
    case QSystemTrayIcon::Trigger:
        /*
         * Событие игнорируется в том случае, если чекбокс не отмечен
         */
        if(cb_roll->isChecked())
        {
            /*
             * иначе, если окно видимо, то оно скрывается,
             * и наоборот, если скрыто, то разворачивается на экран
             */
            if( ! isVisible())
            {
                show();
            }
            else
            {
                hide();
            }
        }
        break;
    default:
        break;
    }
}

/******************************************************************************/
void fm_radio_widget::atexit_slot()
{
    _tb->stop();
    _tb->wait();
}

/******************************************************************************/
void fm_radio_widget::show_wtflow_slot(bool checked)
{
    if(    ! checked
        && _show_waterfall
      )
    /* если команды выключить, и до этого виджет был включен */
    {
        _connect_waterfall_stream(false);
    }
    /* если команда включить, а виджет был отключен */
    else if(
           checked
        && ! _show_waterfall
    )
    {
        _connect_waterfall_stream(true);
    }
    _show_waterfall = checked;
}

/******************************************************************************/
void fm_radio_widget::quit_slot()
{
    QWidget::close();
}

/******************************************************************************/
void fm_radio_widget::change_freq_slot(double new_freq_MHz)
{
    auto freq =   new_freq_MHz
                * 1e6;
    _osmosdr->set_center_freq(freq);
    _waterflw_sink->set_frequency_range(
          freq
        , _rcvr_wb_fs
        );
}

/******************************************************************************/
void fm_radio_widget::change_volume_slot(int val_in_persent)
{
    if(val_in_persent > 100)
    {
        val_in_persent = 100;
    }
    
    if(val_in_persent < 0)
    {
        val_in_persent = 0;
    }
    
    _audio_volume = val_in_persent;
    set_volume((float)val_in_persent);
    cb_mute->setCheckState(Qt::CheckState::Unchecked);
}

/******************************************************************************/
void fm_radio_widget::set_volume(float new_percent_val)
{
    /* некий масштибирующий коэффициент подбирающийся апостериорно */
    static const float _absolute_level_coeff = 0.5;
    /* устанавливаем значение амплитуды в процентах */
    _const_v->set_offset(
                      0.01f
                    * new_percent_val
                    * _absolute_level_coeff
                    );
}

/******************************************************************************/
void fm_radio_widget::mute_audio_slot(bool mute)
{
    auto new_volume =   mute
                      ? 0
                      : _audio_volume;
    set_volume((float)new_volume);
}