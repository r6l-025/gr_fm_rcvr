/* -*- c++ -*- */
/*
 * Copyright 2021 Bolshevik.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_FM_DEMOD_WBFW_DEMOD_CF_H
#define INCLUDED_FM_DEMOD_WBFW_DEMOD_CF_H

#include <fm_demod/api.h>
#include <gnuradio/hier_block2.h>

namespace gr {
namespace fm_demod {

    /**
     * @brief Блок FM демодулятора
     *
     * Является репликой демодулятора из состава GR написанного на python
     *
     * @ingroup fm_demod
     *
     */
    class FM_DEMOD_API wbfw_demod_cf : virtual public gr::hier_block2
    {
    public:
        typedef boost::shared_ptr<wbfw_demod_cf> sptr;

        /**
         * @brief Создать экземпляр класса
         *
         * Метод создает shared_ptr с указателем на объект во измежание
         * использования "сырых" указателей
         *
         * @param quad_rate         - частота дискретизации входноиго
         *                            комплексного потока данных;
         * @param audio_decimation  - порядок децимации входного потока
         */
        static sptr make(
                          float    quad_rate
                        , int      audio_decimation
                        );
    };
    
} // namespace fm_demod
} // namespace gr

#endif /* INCLUDED_FM_DEMOD_WBFW_DEMOD_CF_H */

