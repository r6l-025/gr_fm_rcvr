INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_FM_DEMOD fm_demod)

FIND_PATH(
    FM_DEMOD_INCLUDE_DIRS
    NAMES fm_demod/api.h
    HINTS $ENV{FM_DEMOD_DIR}/include
        ${PC_FM_DEMOD_INCLUDEDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/include
          /usr/local/include
          /usr/include
)

FIND_LIBRARY(
    FM_DEMOD_LIBRARIES
    NAMES gnuradio-fm_demod
    HINTS $ENV{FM_DEMOD_DIR}/lib
        ${PC_FM_DEMOD_LIBDIR}
    PATHS ${CMAKE_INSTALL_PREFIX}/lib
          ${CMAKE_INSTALL_PREFIX}/lib64
          /usr/local/lib
          /usr/local/lib64
          /usr/lib
          /usr/lib64
          )

include("${CMAKE_CURRENT_LIST_DIR}/fm_demodTarget.cmake")

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(FM_DEMOD DEFAULT_MSG FM_DEMOD_LIBRARIES FM_DEMOD_INCLUDE_DIRS)
MARK_AS_ADVANCED(FM_DEMOD_LIBRARIES FM_DEMOD_INCLUDE_DIRS)
