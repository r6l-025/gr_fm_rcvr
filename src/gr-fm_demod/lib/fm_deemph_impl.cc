/* -*- c++ -*- */
/*
 * Copyright 2021 Bolshevik.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* gr includes */
#include <gnuradio/io_signature.h>
#include <gnuradio/filter/iir_filter_ffd.h>

/* local includes */
#include "fm_deemph_impl.h"

namespace gr {
namespace fm_demod {

/******************************************************************************/
fm_deemph::sptr fm_deemph::make(
                              float fd
                            , float tau
                            )
{
    return gnuradio::get_initial_sptr(new fm_deemph_impl(fd, tau));
}

/*
 * Приватный конструктор
 */
/******************************************************************************/
fm_deemph_impl::fm_deemph_impl(
                  float fd
                , float tau
                ) : gr::hier_block2(
                              "fm_deemph"
                            , gr::io_signature::make(1, 1, sizeof(float))
                            , gr::io_signature::make(1, 1, sizeof(float))
                            )
{
    /* сформируем коэффициенты фильтра */
    std::vector<double> fftaps;
    std::vector<double> fbtaps;
    make_iir_coeffs(
                     fd
                   , tau
                   , fftaps
                   , fbtaps
                   );
    
    /* сгенерим фильтр */
    auto iir = filter::iir_filter_ffd::make(
                                          fftaps
                                        , fbtaps
                                        , false
                                        );
    
    connect(
          self()
        , 0
        , iir
        , 0
        );
    connect(
          iir
        , 0
        , self()
        , 0
        );
}

/*
 * Приватный деструктор
 */
/******************************************************************************/
fm_deemph_impl::~fm_deemph_impl()
{
}

/******************************************************************************/
void fm_deemph_impl::make_iir_coeffs(
                          float                 fd
                        , float                 tau
                        , std::vector<double>&  fftaps
                        , std::vector<double>&  fbtaps
                        )
{
    tau += 1e-12f;
    auto w_cd = 1.0 / tau;
    auto w_ca =   2.0
                * fd
                * tan(w_cd / (2.0 * fd));
    
    auto k = -w_ca / (2.0 * fd);
    auto z1 = -1.0;
    auto p1 =   (1.0 + k)
              / (1.0 - k);
    auto b0 =   -k
              / (1.0 - k);
    
    fftaps.push_back(b0 * 1.0);
    fftaps.push_back(b0 * -z1);
    
    fbtaps.push_back(1.0);
    fbtaps.push_back(-p1);
}


} /* namespace fm_demod */
} /* namespace gr */

