/* -*- c++ -*- */
/*
 * Copyright 2021 Bolshevik.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifndef INCLUDED_FM_DEMOD_FM_DEEMPH_IMPL_H
#define INCLUDED_FM_DEMOD_FM_DEEMPH_IMPL_H

#include <fm_demod/fm_deemph.h>

namespace gr {
namespace fm_demod {

class fm_deemph_impl : public fm_deemph
{
private:

public:
    /**
     * @brief приватный конструктор класса
     *
     * @param fd   - частота дискретизации входноиго потока данных;
     * @param tau  - постоянная времени фильтра
     */
    fm_deemph_impl(
              float fd
            , float tau
            );
    ~fm_deemph_impl();

    void make_iir_coeffs(
              float                 fd
            , float                 tau
            , std::vector<double>&  fftaps
            , std::vector<double>&  fbtaps
            );
};

} // namespace fm_demod
} // namespace gr

#endif /* INCLUDED_FM_DEMOD_FM_DEEMPH_IMPL_H */

