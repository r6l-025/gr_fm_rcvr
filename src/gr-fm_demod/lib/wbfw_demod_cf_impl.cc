/* -*- c++ -*- */
/*
 * Copyright 2021 Bolshevik.
 *
 * This is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3, or (at your option)
 * any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this software; see the file COPYING.  If not, write to
 * the Free Software Foundation, Inc., 51 Franklin Street,
 * Boston, MA 02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

/* gr includes */
#include <gnuradio/io_signature.h>
#include <gnuradio/analog/quadrature_demod_cf.h>
#include <gnuradio/filter/firdes.h>
#include <gnuradio/filter/fir_filter_blk.h>

/* local includes */
#include "wbfw_demod_cf_impl.h"
#include "fm_demod/fm_deemph.h"

namespace gr {
namespace fm_demod {

/******************************************************************************/
wbfw_demod_cf::sptr wbfw_demod_cf::make(
                                      float    quad_rate
                                    , int      audio_decimation
                                    )
{
    return gnuradio::get_initial_sptr(
                                new wbfw_demod_cf_impl(
                                      quad_rate
                                    , audio_decimation
                                    )
                                );
}


/*
 * Приватный конструктор
 */
/******************************************************************************/
wbfw_demod_cf_impl::wbfw_demod_cf_impl(
                              float    quad_rate
                            , int      audio_decimation
                            ) : gr::hier_block2(
                              "wbfw_demod_cf"
                            , gr::io_signature::make(1, 1, sizeof(gr_complex))
                            , gr::io_signature::make(1, 1, sizeof(float))
                            )
{
    const float max_dev         = 75e3;
    const float fm_demod_gain   =   quad_rate
                                  / (2 * (float)M_PI * max_dev);
    const float audio_rate      =   quad_rate
                                  / (float)audio_decimation;
    
    /* демодулятор */
    auto quadr_demod = analog::quadrature_demod_cf::make(fm_demod_gain);
    /* фильтр корректировки */
    auto deemph = fm_deemph::make(audio_rate);
    /* выходной аудио-фильтр */
    auto band_transition_width = audio_rate / 32;
    auto fir_coeff = filter::firdes::low_pass(
                                          /* усиление */
                                          1.0
                                          /* частота дискретизации */
                                        , quad_rate
                                          /* частота среза фильтра */
                                        , audio_rate / 2 - band_transition_width
                                          /* ширина переходной полосы */
                                        , band_transition_width
                                        , filter::firdes::win_type::WIN_HAMMING
                                        );
    auto fir = filter::fir_filter_fff::make(
                                          audio_decimation
                                        , fir_coeff
                                        );
        
    /*
     *            +---------+   +------------+   +---------+
     *      IN -->| Q-Demod |-->| Deemphasis |-->|   FIR   |--> OUT
     *            +---------+   +------------+   +---------+
     */
    connect(self()      , 0, quadr_demod, 0);
    connect(quadr_demod , 0, deemph     , 0);
    connect(deemph      , 0, fir        , 0);
    connect(fir         , 0, self()     , 0);
}

/*
 * Приватный деструктор
 */
/******************************************************************************/
wbfw_demod_cf_impl::~wbfw_demod_cf_impl()
{
}


} /* namespace fm_demod */
} /* namespace gr */

